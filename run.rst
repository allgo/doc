.. _run:

Run a Job
---------



.. _parameters:

Parameters
^^^^^^^^^^

Application often needs parameters. These one can be forwarded by a string to the application.
In order to facilitate their usage, some presets can be defined by the author . If you select one of them in the web form, the entry field will be automatically filled.
Otherwise, the application documentation should help.


.. _queue-type-ref:

Queue type
^^^^^^^^^^

A job will be executed in a given queue depending of the time processing. The
system has 3 queue types:

- Interactive: less than a minute
- Standard: less than 20 minutes
- batch: less than a day

.. warning::

  If a job lasts more than the queue max duration, it will time out and be
  aborted.


.. _quotas-ref:

Quotas
^^^^^^
User can be limited by quotas.
For exemple, the samusa audio segmentation tools is limit to 50 MB of audio files.
If you need more, please contact the author of the application.
