Changelog
=========

30 Aug 2023
-----------

 * Jobs parameters may now contain special characters if allowed by the app
   owner.
    - The default behaviour is unchanged: allgo forbids all special symbols
      that may be interpreted by the shell (to protect against code injections)
      and the ``/`` symbol (to protect against directory traversal).
    - Owners may lift the restrictions on a per-symbol basis, provided that the
      app inputs are properly sanitised.

01 Dec 2022
-----------

 * REST API
    - new endpoints for :ref:`aborting <api-abort>` and
      :ref:`deleting <api-delete>` jobs
    - allow selecting a specific webapp version on
      :ref:`job submission <api-create>`
    - improve error messages
    - fixes in the CORS configuration
 * Improve login/logout UX
    - return to the same page after login/logout
    - remove intermediate pages for login/logout
 * Limit job lifetime (automatic deletion after 30 days)
 * Hide the job creation form when the webapp has no published version yet
 * Fix possible error 404 on job deletion
 * Fix job OOM reporting

10 May 2021
-----------

 * New `Metrics <https://allgo18.inria.fr/metrics>`_ feature. Give you acces to
   the statistics of your application. 

   It is now possible to see Metrics of your application according to the following criterias:
     - Application name
     - Time period
     - Group by day/month/year
     
 * Allgo API has been upgrade to integrate "Metrics"
     - /api/v1/metrics/

23 Apr 2020
-----------

 * The internal docker registry is now exposed to the user to allow
   continuous deployment. You can :ref:`build your webapp as a docker image and
   push it to Allgo <deploy-docker>`.
 * A page was added for :ref:`managing the versions of an application
   <manage-versions>`.
   It is now possible to:
     - rename/delete/restore a deployed version (deleted versions
       remain accessible during 30 days)
     - mark a version as 'unpublished' or 'published'. An 'unpublished'
       version is not visible by the other users (even if the app is
       public), you can use this feature for your work-in-progress
       versions.
 * The list of job input files is now provided in the :ref:`environment
   variable ALLGO_FILES<entrypoint>`. We added it for two reasons: to avoid
   forcing the entrypoint to discover the input files (which was a bit awkward
   and error-prone) and to tell in which order the files are submitted by the
   user.
 * The job submission form now allows submitting files from different
   directories and it supports drag'n'drop.
 * There is new :ref:`API endpoint for monitoring the progress of a job
   <api-monitor>` without polling
 * Many bugfixes, especially in the API (CORS config, divergences with the
   original API from https://allgo.inria.fr/)


14 Oct 2019
-----------
 * API fixes (the response of *POST /apt/v1/jobs* diverged from the original API)

08 Jan 2019
-----------

 * fixed the zip file generation (job files)
 * minor UI fixes

20 Dec 2018
-----------

New allgo frontend
