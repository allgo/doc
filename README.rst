========================
A||Go user documentation
========================

To be compiled, it requires:

- Sphinx
- Sphinx RTD theme

The documentation can been seen at https://allgo.gitlabpages.inria.fr/doc/.

************
Requirements
************
  | You need to have python3 and pip3 installed on your machine.
  | We encourage you to use virtualenv

Installation
============

facultative
-----------
.. code-block:: bash
   :linenos:

   VENV_PATH=$HOME/venv/doc_allgo
   pip install virtualenv
   virtualenv $VENV_PATH
   source $VENV_PATH/bin/activate

install dependencies
--------------------
.. code-block:: bash
   :linenos:

   pip install -r requirements.txt

Build the doc
=============
.. code-block:: bash
   :linenos:

   make html

You can type `make` to get the help, and information about the various format available.
