.. _notebook:

Notebook integration
=====================

We will explain in this document how to use AllGO with a python notebook.

An example is available here : https://gitlab.inria.fr/allgo/notebooks/ndsafir


1. Install allgo module 
-----------------------

The old client library is available at https://pypi.org/project/allgo/

Install it with :

.. code-block:: python

	pip install git+https://gitlab.inria.fr/allgo/api-clients/python_client.git


2. Create an application
------------------------

.. code-block:: python

	import allgo
	print(allgo.__version__)
	# Display allgo API doc
	print(allgo.__doc__)
	client = allgo.Client(my_private_token)

.. note::
	token in optional, if you already provide your token with an env variable ALLGO_TOKEN or create a file ~/.allgo_token (without breakline)

3. Submit a job : 
-----------------

.. code-block:: python

	the_files = {'files[0]': open('tmp.png', 'rb')}
	the_params = '-nopeaks 1 -2dt false -noise 0 -p 1 -bits 8 -iter 5 -adapt 0'
	out_dict1 = client.create_job(the_app_number, params=the_params, files=the_files)
	out_dict2 = client.job_status(out_dict1['id'])


In this example, we send one file 'tmp.png' and a string of parameters 'params'.
Run is blocking, when finish all files produce by A||Go are downloaded in the current directory.
the_app_number (`ndsafir`) application create a file `output.png` in that case, so display the result with :

.. code-block:: python

	import matplotlib.pyplot as plt
	img = mpimg.imread('output.png')
	plt.imshow(img)


Complex GUI
===========

You will discover in this `example <https://gitlab.inria.fr/allgo/notebooks/ndsafir>`_ more complex GUI with `IPyWidgets <https://ipywidgets.readthedocs.io/en/stable/>`_

.. image:: https://gitlab.inria.fr/allgo/notebooks/ndsafir/raw/master/images/illustration_interactif.png




