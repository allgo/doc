Welcome to A||GO
================

Allgo is a Scientific Software As A Service (:term:`SAAS`) platform.

Introduction
------------

Users can **upload** their data after choosing the software they wanted to test.
Once the data is processed, the user can **download the results**.

For instance, `Samusa <http://allgo.inria.fr/app/samusa>`_ is an algorithm for detecting music and speech in an audio file.
Upload an :term:`MP3` file and obtain a text file containing all the timestamps of each kind of segment.

A web interface and :ref:`api` are provided in order to test and integrate A||Go with your development.

From a provisioning point of view, several methods are available (see :ref:`deploy`) to make your software deployement as easy as possible.

Contact : allgo@inria.fr


User's guide
------------
=======
Quickstart
----------

Prerequisites : create an account and validate it by email.

1. Choose your `application <https://allgo18.inria.fr/apps/>`_

.. image:: _static/app_allgo.png
   :width: 500px

2. Submit the job (input files + command line parameters)

.. image:: _static/app_job.png
   :width: 500px

3 Download your results

.. image:: _static/app_results.png
   :width: 500px



Table of contents
-----------------

.. toctree::
   :maxdepth: 2

   deploy
   run
   api
   notebook
   faq
   glossary
   changelog
