Frequently Asked Questions
==========================

I found an issue or have a request, how can I report it ?
---------------------------------------------------------

If you found a bug or any issue, you can report it on the `Allgo repository`_. For support requests, you may contact us in private on `INRIA Helpdesk`_ or `by email`_.

.. _INRIA Gitlab account: http://gitlab.inria.fr/
.. _INRIA Helpdesk: https://helpdesk.inria.fr/categories/227/submit
.. _by email: allgo@inria.fr
.. _Allgo repository: https://gitlab.inria.fr/allgo/allgo.inria.fr/issues


How long my files stay in the plateform ?
-----------------------------------------

Job older than **one month** are deleted.


What about my data ?
--------------------

Your data cannot be used outside the institute. They may be used only for debug of the application by the author.


What about the roadmap ? 
------------------------

This new version A||GO18 is almost isofunctional except for documentation and demonstrators because we will integrate notebooks with `Jupyter <https://jupyter.org/>`_ into the platform.
Indeed, notebooks are a most approriate tool for this kind of usage, you will have better GUI widgets, portability and reliability.

Notebook are planned to be integrated in Q1 2019.

We also looks in the `public issue tracker <https://gitlab.inria.fr/allgo/allgo.inria.fr/issues>`_ to schedule our developpements.
The job progress feature will probably be included in this release also.
